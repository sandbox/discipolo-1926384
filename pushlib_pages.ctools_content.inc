<?php
/**
 * @file
 * pushlib_pages.ctools_content.inc
 */

/**
 * Implements hook_default_ctools_custom_content().
 */
function pushlib_pages_default_ctools_custom_content() {
  $export = array();

  $content = new stdClass();
  $content->disabled = FALSE; /* Edit this to true to make a default content disabled initially */
  $content->api_version = 1;
  $content->name = 'additional_contact_info';
  $content->admin_title = 'additional contact info';
  $content->admin_description = 'pane for the contact page';
  $content->category = 'contact';
  $content->settings = array(
    'admin_title' => '',
    'title' => 'you can also contact us',
    'body' => 'by writing us a postcard to .....',
    'format' => 'plain_text',
    'substitute' => 1,
  );
  $export['additional_contact_info'] = $content;

  return $export;
}
