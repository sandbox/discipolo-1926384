<?php
/**
 * @file
 * pushlib_pages.features.uuid_node.inc
 */

/**
 * Implements hook_uuid_features_default_content().
 */
function pushlib_pages_uuid_features_default_content() {
  $nodes = array();

  $nodes[] = array(
  'uid' => '1',
  'title' => 'About',
  'log' => '',
  'status' => '1',
  'comment' => '1',
  'promote' => '0',
  'sticky' => '0',
  'vuuid' => 'b03f9696-5344-34e4-e5cb-baca9cac37f6',
  'type' => 'page',
  'language' => 'und',
  'created' => '1361555533',
  'tnid' => '0',
  'translate' => '0',
  'uuid' => '5b0afaa7-6865-95b4-257d-b3478397728c',
  'revision_uid' => '1',
  'body' => array(
    'und' => array(
      0 => array(
        'value' => 'We are ...',
        'summary' => '',
        'format' => 'plain_text',
        'safe_value' => '<p>We are ...</p>
',
        'safe_summary' => '',
      ),
    ),
  ),
  'cid' => '0',
  'last_comment_name' => NULL,
  'last_comment_uid' => '1',
  'comment_count' => '0',
  'name' => 'admin',
  'picture' => '0',
  'data' => 'b:0;',
  'date' => '2013-02-22 18:52:13 +0100',
);
  $nodes[] = array(
  'uid' => '1',
  'title' => 'Our Services and offers',
  'log' => '',
  'status' => '1',
  'comment' => '1',
  'promote' => '0',
  'sticky' => '0',
  'vuuid' => 'cbdd26cb-735f-1654-61d7-5d89e8772836',
  'type' => 'page',
  'language' => 'und',
  'created' => '1361556902',
  'tnid' => '0',
  'translate' => '0',
  'uuid' => '8a0a4a5c-3174-2804-4d43-bb7c6a133e4a',
  'revision_uid' => '1',
  'body' => array(
    'und' => array(
      0 => array(
        'value' => 'blah',
        'summary' => '',
        'format' => 'plain_text',
        'safe_value' => '<p>blah</p>
',
        'safe_summary' => '',
      ),
    ),
  ),
  'cid' => '0',
  'last_comment_name' => NULL,
  'last_comment_uid' => '1',
  'comment_count' => '0',
  'name' => 'admin',
  'picture' => '0',
  'data' => 'b:0;',
  'date' => '2013-02-22 19:15:02 +0100',
);
  $nodes[] = array(
  'uid' => '1',
  'title' => 'Impressum',
  'log' => '',
  'status' => '1',
  'comment' => '1',
  'promote' => '0',
  'sticky' => '0',
  'vuuid' => '4e6313e2-57db-f794-c93d-48b20d5bb693',
  'type' => 'page',
  'language' => 'und',
  'created' => '1361556877',
  'tnid' => '0',
  'translate' => '0',
  'uuid' => 'ee484561-6469-fc64-b52b-b5950c37086a',
  'revision_uid' => '1',
  'body' => array(),
  'cid' => '0',
  'last_comment_name' => NULL,
  'last_comment_uid' => '1',
  'comment_count' => '0',
  'name' => 'admin',
  'picture' => '0',
  'data' => 'b:0;',
  'date' => '2013-02-22 19:14:37 +0100',
);
  return $nodes;
}
